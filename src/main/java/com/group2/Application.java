package com.group2;

import com.group2.model.bouquets.Bouquet;
import com.group2.model.bouquets.DecorType;
import com.group2.model.bouquets.FlowerType;
import com.group2.model.bouquets.bouquetImplementation.BirthdayBouquet;
import com.group2.model.bouquets.decorators.DecorDecorator;
import com.group2.model.bouquets.decorators.FlowerDecorator;
import com.group2.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Application {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(Application.class);
        View view = new View();
        view.start();

    }
}
