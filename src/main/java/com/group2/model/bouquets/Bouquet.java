package com.group2.model.bouquets;

import java.util.List;

public interface Bouquet {
     List<Decor> getDecorations();
     List<Flower> getFlowers();
}
