package com.group2.model.bouquets;


import java.util.List;

public abstract class BouquetDecorator implements Bouquet {
    protected Bouquet bouquet;
    protected List<Flower> flowers;
    protected List<Decor> decorations;


    public void setBouquet(Bouquet bouquet, FlowerType ft) {

        this.bouquet = bouquet;
    }

    public void setBouquet(Bouquet bouquet, DecorType dt) {

        this.bouquet = bouquet;
    }

    @Override
    public List<Decor> getDecorations() {
        return bouquet.getDecorations();
    }
    @Override
    public List<Flower> getFlowers() {
        return bouquet.getFlowers();
    }
}
