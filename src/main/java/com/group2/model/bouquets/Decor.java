package com.group2.model.bouquets;

public class Decor {
    private DecorType decortype;
    public Decor(DecorType dt){
        this.decortype = dt;
    }

    @Override
    public String toString(){
        return decortype.toString()+" ";
    }
}
