package com.group2.model.bouquets;

public class Flower {
    private FlowerType flowerType;
    public Flower(FlowerType ft){
        this.flowerType = ft;
    }
    @Override
    public String toString(){
        return flowerType.toString()+" ";
    }
}
