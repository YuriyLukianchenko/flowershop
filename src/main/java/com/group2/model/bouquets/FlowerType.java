package com.group2.model.bouquets;

public enum FlowerType {
    ROSE, CHAMOMILE, TULIP, LILY, MARIGOLD
}
