package com.group2.model.bouquets.bouquetImplementation;

import com.group2.model.bouquets.*;

import java.util.LinkedList;
import java.util.List;

public class BirthdayBouquet implements Bouquet {
    private List<Flower> flowers;
    private List<Decor> decorations;
    public BirthdayBouquet(){
        flowers = new LinkedList<>();
        flowers.add(new Flower(FlowerType.CHAMOMILE));
        flowers.add(new Flower(FlowerType.CHAMOMILE));
        flowers.add(new Flower(FlowerType.CHAMOMILE));
        flowers.add(new Flower(FlowerType.TULIP));
        flowers.add(new Flower(FlowerType.TULIP));
        flowers.add(new Flower(FlowerType.TULIP));
        flowers.add(new Flower(FlowerType.TULIP));
        decorations = new LinkedList<>();
        decorations.add(new Decor(DecorType.GREEN_BOW));
        decorations.add(new Decor(DecorType.YELLOW_WRAPPER));
    }
    public List<Decor> getDecorations(){
       return decorations;
    }
    public List<Flower> getFlowers(){
        return flowers;
    }
    @Override
    public String toString(){
        return "\n Buequet: \n" + "flowers: " + flowers + "\n" + "decorations: " + decorations;
    }
}
