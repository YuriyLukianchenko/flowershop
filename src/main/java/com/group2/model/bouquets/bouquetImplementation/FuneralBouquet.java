package com.group2.model.bouquets.bouquetImplementation;

import com.group2.model.bouquets.*;

import java.util.LinkedList;
import java.util.List;

public class FuneralBouquet implements Bouquet {
    private List<Flower> flowers;
    private List<Decor> decorations;
    public FuneralBouquet(){
        flowers = new LinkedList<>();
        flowers.add(new Flower(FlowerType.ROSE));
        flowers.add(new Flower(FlowerType.ROSE));
        flowers.add(new Flower(FlowerType.ROSE));
        flowers.add(new Flower(FlowerType.ROSE));
        flowers.add(new Flower(FlowerType.ROSE));
        flowers.add(new Flower(FlowerType.ROSE));
        flowers.add(new Flower(FlowerType.ROSE));
        flowers.add(new Flower(FlowerType.ROSE));
        decorations = new LinkedList<>();
        decorations.add(new Decor(DecorType.BLACK_WRAPPER));
    }
    public List<Decor> getDecorations(){
        return decorations;
    }
    public List<Flower> getFlowers(){
        return flowers;
    }
    @Override
    public String toString(){
        return "\n Buequet: \n" + "flowers: " + flowers + "\n" + "decorations: " + decorations;
    }
}
