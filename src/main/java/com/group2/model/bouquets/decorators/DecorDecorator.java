package com.group2.model.bouquets.decorators;

import com.group2.model.bouquets.Bouquet;
import com.group2.model.bouquets.BouquetDecorator;
import com.group2.model.bouquets.Decor;
import com.group2.model.bouquets.DecorType;

public class DecorDecorator extends BouquetDecorator {
    public void setBouquet(Bouquet b, DecorType dt){
        bouquet = b;
        bouquet.getDecorations().add(new Decor(dt));
    }
    public String toString(){
        return "\n Buequet: \n" + "flowers: " + bouquet.getFlowers() + "\n" + "decorations: " +bouquet.getDecorations();
    }
}
