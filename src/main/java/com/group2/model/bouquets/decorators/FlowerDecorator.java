package com.group2.model.bouquets.decorators;

import com.group2.model.bouquets.Bouquet;
import com.group2.model.bouquets.BouquetDecorator;
import com.group2.model.bouquets.Flower;
import com.group2.model.bouquets.FlowerType;

public class FlowerDecorator extends BouquetDecorator {
    public void setBouquet(Bouquet b, FlowerType ft){
        bouquet = b;
        bouquet.getFlowers().add(new Flower(ft));
    }
    public String toString(){
        return "\n Buequet: \n" + "flowers: " + bouquet.getFlowers() + "\n" + "decorations: " +bouquet.getDecorations();
    }
}
