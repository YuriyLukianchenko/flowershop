package com.group2.model.clientBase;

import com.group2.model.orders.ordersbycurrency.OrderInHryvnias;
import com.group2.model.currencyAdapter.moneytype.UkrainianHryvnia;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Client implements Serializable {

    private String id;
    private Type type;
    private int orderCounter;
    private double orderPrice;
    private UkrainianHryvnia hryvniaBalance;
    private List<OrderInHryvnias> orders;

    public Client(String id, Type type, UkrainianHryvnia hryvniaBalance) {
        this.id = id;
        this.type = type;
        this.hryvniaBalance = hryvniaBalance;
        orderPrice = 0;
        orderCounter = 0;
        orders = new LinkedList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getOrderCounter() {
        return orderCounter;
    }

    public void setOrderCounter(int orderCounter) {
        this.orderCounter = orderCounter;
    }

    public double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public void addOrder(double price) {
        orderPrice += price;
        orderCounter += 1;
        if (orderPrice > 100) {
            type = Type.GOLD;
        } else if (orderPrice > 200) {
            type = Type.VIP;
        } else {
            type = Type.STANDART;
        }
    }

    public boolean buyProduct(OrderInHryvnias order) {
        boolean result = hryvniaBalance.subtract(order.getPrice());
        if (result) {
            orders.add(order);
        }
        return result;
    }

    public List<OrderInHryvnias> getOrders() {
        return orders;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", hryvniaBalance=" + hryvniaBalance +
                '}' + "\n";
    }
}
