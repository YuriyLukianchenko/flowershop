package com.group2.model.clientBase;

import com.group2.model.currencyAdapter.moneytype.UkrainianHryvnia;
import com.group2.model.exceptions.ClientExistAllreadyException;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class ClientBase implements Serializable {

    public Map<String, Client> clientList;

    public ClientBase() {
        this.clientList = new HashMap<>();
    }

    public void addClient(String id) throws ClientExistAllreadyException {
        if (!clientList.containsKey(id)) {
            clientList.put(id, new Client(id, Type.STANDART, new UkrainianHryvnia(30_000, 0)));
        } else {
            throw new ClientExistAllreadyException();
        }
    }

    public Client chooseClient(String clientID) {
        return clientList.get(clientID);
    }

    public void removeClient(Client client) {
        clientList.remove(client);
    }

    @Override
    public String toString() {
        return "ClientBase{" +
                "clientList=" + clientList +
                '}';
    }
}
