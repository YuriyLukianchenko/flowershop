package com.group2.model.clientBase;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializationBase {

    public ClientBase deserializationClientBase(){
        ClientBase base = null;
        try (ObjectInputStream objectInputStream =
                     new ObjectInputStream(
                             new FileInputStream("client_base.dat"))){
            base = (ClientBase) objectInputStream.readObject();
        } catch (FileNotFoundException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return base;
    }
}
