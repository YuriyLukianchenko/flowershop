package com.group2.model.clientBase;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationBase {

    public void serializationClientBase(ClientBase base) {
        try (ObjectOutputStream objectOutputStream =
                     new ObjectOutputStream(
                             new FileOutputStream("client_base.dat"))) {
            objectOutputStream.writeObject(base);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
