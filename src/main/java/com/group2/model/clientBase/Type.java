package com.group2.model.clientBase;

import java.io.Serializable;

public enum Type implements Serializable {
    STANDART, GOLD, VIP
}
