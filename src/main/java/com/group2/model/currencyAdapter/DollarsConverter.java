package com.group2.model.currencyAdapter;

import com.group2.model.currencyAdapter.moneytype.USDollar;
import com.group2.model.currencyAdapter.moneytype.UkrainianHryvnia;
import com.group2.model.orders.ordersbycurrency.OrderInDollars;
import com.group2.model.orders.ordersbycurrency.OrderInHryvnias;

public class DollarsConverter implements OrderInHryvnias {
    private double courseHryvniaPerDollar;
    private OrderInDollars orderInDollars;

    public DollarsConverter(double courseHryvniaPerDollar, OrderInDollars orderInDollars) {
        this.courseHryvniaPerDollar = courseHryvniaPerDollar;
        this.orderInDollars = orderInDollars;
    }

    @Override
    public String showParameters() {
        return orderInDollars.showParameters();
    }

    @Override
    public UkrainianHryvnia getPrice() {
        USDollar dollar = orderInDollars.getPrice();
        int priceCent = dollar.getDollar() * 100 + dollar.getCent();
        int priceKopiyka = (int) (priceCent * courseHryvniaPerDollar);
        return new UkrainianHryvnia(priceKopiyka / 100, priceKopiyka % 100);
    }
}
