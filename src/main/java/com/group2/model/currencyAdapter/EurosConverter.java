package com.group2.model.currencyAdapter;

import com.group2.model.currencyAdapter.moneytype.Euro;
import com.group2.model.currencyAdapter.moneytype.UkrainianHryvnia;
import com.group2.model.orders.ordersbycurrency.OrderInEuros;
import com.group2.model.orders.ordersbycurrency.OrderInHryvnias;

public class EurosConverter implements OrderInHryvnias {
    private double courseHryvniaPerEuro;
    private OrderInEuros orderInEuros;

    public EurosConverter(double courseHryvniaPerEuro, OrderInEuros orderInEuros) {
        this.courseHryvniaPerEuro = courseHryvniaPerEuro;
        this.orderInEuros = orderInEuros;
    }

    @Override
    public String showParameters() {
        return orderInEuros.showParameters();
    }

    @Override
    public UkrainianHryvnia getPrice() {
        Euro euro = orderInEuros.getPrice();
        int priceCent = euro.getEuro() * 100 + euro.getCent();
        int priceKopiyka = (int) (priceCent * courseHryvniaPerEuro);
        return new UkrainianHryvnia(priceKopiyka / 100, priceKopiyka % 100);
    }

}
