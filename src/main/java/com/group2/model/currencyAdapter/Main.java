package com.group2.model.currencyAdapter;

import com.group2.model.clientBase.Client;
import com.group2.model.currencyAdapter.moneytype.Euro;
import com.group2.model.currencyAdapter.moneytype.USDollar;
import com.group2.model.currencyAdapter.moneytype.UkrainianHryvnia;
import com.group2.model.orders.ordersbycurrency.DollarsOrder;
import com.group2.model.orders.ordersbycurrency.EurosOrder;
import com.group2.model.orders.ordersbycurrency.HryvniasOrder;
import com.group2.model.orders.ordersbycurrency.OrderInHryvnias;

import static com.group2.model.clientBase.Type.GOLD;

public class Main {
    public static void main(String[] args) {
        Client tarasBabii = new Client("Taras Babii", GOLD, new UkrainianHryvnia(1000, 0));

        System.out.println(tarasBabii);
        System.out.println();

        HryvniasOrder hryvniasOrder1 = new HryvniasOrder(new UkrainianHryvnia(250, 0), "Black roses");
        HryvniasOrder hryvniasOrder2 = new HryvniasOrder(new UkrainianHryvnia(150, 0), "Red tulips");

        tarasBabii.buyProduct(hryvniasOrder1);
        tarasBabii.buyProduct(hryvniasOrder2);

        printOrders(tarasBabii);

        DollarsOrder dollarsOrder = new DollarsOrder(new USDollar(10, 0), "Package of weed");
        DollarsConverter dollarsConverter = new DollarsConverter(26.5, dollarsOrder);
        tarasBabii.buyProduct(dollarsConverter);

        printOrders(tarasBabii);

        EurosOrder eurosOrder = new EurosOrder(new Euro(10, 0), "Opium poppy");
        EurosConverter eurosConverter = new EurosConverter(29.5, eurosOrder);
        tarasBabii.buyProduct(eurosConverter);

        printOrders(tarasBabii);
    }

    private static void printOrders(Client tarasBabii) {
        for (OrderInHryvnias order : tarasBabii.getOrders()) {
            System.out.println(order.showParameters());
        }
        System.out.println();
        System.out.println(tarasBabii);
        System.out.println();
    }
}
