package com.group2.model.currencyAdapter.moneytype;

public class Euro {
    private int euro;
    private int cent;

    public Euro(int euro, int cent) {
        if (euro < 0 || cent < 0) {
            throw new IllegalArgumentException("Euro or Cent has an invalid value");
        }
        this.euro = euro;
        this.cent = cent;
    }

    public int getEuro() {
        return euro;
    }

    public int getCent() {
        return cent;
    }

    public boolean subtract(Euro value) {
        int temp = (euro * 100 + cent) - (value.getEuro() * 100 + value.getCent());
        if (temp < 0) {
            return false;
        } else {
            euro = temp / 100;
            cent = temp % 100;
            return true;
        }
    }

    @Override
    public String toString() {
        return "" + euro + "." + cent + " euro";
    }
}
