package com.group2.model.currencyAdapter.moneytype;

public class USDollar {
    private int dollar;
    private int cent;

    public USDollar(int dollar, int cent) {
        if (dollar < 0 || cent < 0) {
            throw new IllegalArgumentException("Dollar or Cent has an invalid value");
        }
        this.dollar = dollar;
        this.cent = cent;
    }

    public int getDollar() {
        return dollar;
    }

    public int getCent() {
        return cent;
    }

    public boolean subtract(USDollar value) {
        int temp = (dollar * 100 + cent) - (value.getDollar() * 100 + value.getCent());
        if (temp < 0) {
            return false;
        } else {
            dollar = temp / 100;
            cent = temp % 100;
            return true;
        }
    }

    @Override
    public String toString() {
        return "" + dollar + "." + cent + " dol";
    }
}
