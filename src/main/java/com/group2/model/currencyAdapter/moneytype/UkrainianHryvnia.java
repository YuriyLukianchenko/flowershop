package com.group2.model.currencyAdapter.moneytype;

import java.io.Serializable;

public class UkrainianHryvnia implements Serializable {
    private int hryvnia;
    private int kopiyka;

    public UkrainianHryvnia(int hryvnia, int kopiyka) {
        if (hryvnia < 0 || kopiyka < 0 || kopiyka > 100) {
            throw new IllegalArgumentException("Hryvnia or Kopiyka has an invalid value");
        }
        this.hryvnia = hryvnia;
        this.kopiyka = kopiyka;
    }

    public int getHryvnia() {
        return hryvnia;
    }

    public int getKopiyka() {
        return kopiyka;
    }

    public boolean subtract(UkrainianHryvnia value) {
        int temp = (hryvnia * 100 + kopiyka) - (value.getHryvnia() * 100 + value.getKopiyka());
        if (temp < 0) {
            return false;
        } else {
            hryvnia = temp / 100;
            kopiyka = temp % 100;
            return true;
        }
    }

    @Override
    public String toString() {
        return "" + hryvnia + "." + kopiyka + " grn";
    }
}
