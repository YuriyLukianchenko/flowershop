package com.group2.model.observer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerObserver implements Observer {
    private static final Logger LOGGER = LogManager.getLogger(LoggerObserver.class);

    @Override
    public void handleEvent(String event) {
        LOGGER.debug("Your order's status is: " + event);
    }
}
