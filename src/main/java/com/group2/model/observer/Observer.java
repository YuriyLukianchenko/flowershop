package com.group2.model.observer;

public interface Observer {
    void handleEvent(String event);
}
