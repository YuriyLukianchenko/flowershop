package com.group2.model.orderStates;

public class Cancelling implements OrderState{

    @Override
    public String toString() {
        return "{Cancelling order}";
    }
}
