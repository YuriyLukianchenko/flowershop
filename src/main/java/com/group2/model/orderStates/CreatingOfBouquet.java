package com.group2.model.orderStates;

import com.group2.model.bouquets.Bouquet;
import com.group2.model.bouquets.DecorType;
import com.group2.model.bouquets.FlowerType;
import com.group2.model.bouquets.bouquetImplementation.BirthdayBouquet;
import com.group2.model.bouquets.bouquetImplementation.FuneralBouquet;
import com.group2.model.bouquets.bouquetImplementation.ValentineBouquet;
import com.group2.model.bouquets.bouquetImplementation.WeddingBouquet;
import com.group2.model.bouquets.decorators.DecorDecorator;
import com.group2.model.bouquets.decorators.FlowerDecorator;
import com.group2.model.observer.Observed;
import com.group2.model.observer.Observer;
import com.group2.model.orders.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class CreatingOfBouquet implements OrderState, Observed {
    Logger logger = LogManager.getLogger(CreatingOfBouquet.class);
    private List<Observer> observers = new ArrayList<>();
    private String status = "";

    @Override
    public void showBouquet(Order order){
        logger.info(order.getCurrentBouquet().toString());
    }
    public void addBouquetToOrder(Order order1){
        Bouquet transitBouquet = order1.getCurrentBouquet();
        order1.getBouquets().add(transitBouquet);
        if( transitBouquet instanceof WeddingBouquet ){
            logger.info("ooooooooooooooooooooooooooooooo");
            order1.currentBouquet = null; //kopiya of order?????? doesn't work !!!
        }
        if( transitBouquet instanceof FuneralBouquet){
            logger.info("ffffffffffffffffffffffffffff");
            order1.setCurrentBouquet(new FuneralBouquet());
        }
        if( transitBouquet instanceof BirthdayBouquet){
            order1.setCurrentBouquet(new BirthdayBouquet());
        }
        if( transitBouquet instanceof ValentineBouquet){
            order1.setCurrentBouquet(new ValentineBouquet());
        }

    }

    public void showOrder(Order order){
        order.getBouquets().stream().forEach(logger::info);
    }
    public void decorateByString(Order order){
        DecorDecorator dd1 = new DecorDecorator();
        dd1.setBouquet(order.getCurrentBouquet(), DecorType.GOLD_STRING);
        order.setCurrentBouquet(dd1);
    }
    public void decorateByFlower(Order order){
        FlowerDecorator fd1 = new FlowerDecorator();
        fd1.setBouquet(order.getCurrentBouquet(), FlowerType.MARIGOLD);
        order.setCurrentBouquet(fd1);
    }

    @Override
    public void submit(Order order) {
        order.setOrderState(new OrderChecking());
        this.status = "submitted";
        notifyObservers();
    }

    @Override
    public void cancel(Order order) {
        order.setOrderState(new Cancelling());
        this.status = "canceled";
        notifyObservers();

    }

    @Override
    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.stream().forEach(ob -> ob.handleEvent(status));
    }

    @Override
    public String toString() {
        return "{Creating of bouquet}";
    }
}
