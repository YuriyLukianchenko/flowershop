package com.group2.model.orderStates;

public class Done implements OrderState{
    @Override
    public String toString() {
        return "{Done order}";
    }
}
