package com.group2.model.orderStates;

import com.group2.model.observer.Observed;
import com.group2.model.observer.Observer;
import com.group2.model.orders.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderChecking implements OrderState, Observed {
    private List<Observer> observers = new ArrayList<>();
    private String status = "";

    @Override
    public void grant(Order order) {
        order.setOrderState(new WaitingForPayment());
        this.status = "granted";
        notifyObservers();
    }

    @Override
    public void cancel(Order order) {
        order.setOrderState(new Cancelling());
        this.status = "canceled";
        notifyObservers();
    }

    @Override
    public void toCreatingOfBouquet(Order order) {
        order.setOrderState(new CreatingOfBouquet());
    }

    @Override
    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.stream().forEach(ob -> ob.handleEvent(status));
    }

    @Override
    public String toString() {
        return "{Order checking}";
    }
}
