package com.group2.model.orderStates;

import com.group2.model.observer.Observed;
import com.group2.model.observer.Observer;
import com.group2.model.orders.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderPreparing implements OrderState, Observed {

    private List<Observer> observers = new ArrayList<>();

    @Override
    public void prepareOrder(Order order) {
        order.setOrderState(new Shipping());
        notifyObservers();
    }

    @Override
    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.stream().forEach(ob -> ob.handleEvent("prepared"));
    }

    @Override
    public String toString() {
        return "{Order preparing}";
    }
}
