package com.group2.model.orderStates;

import com.group2.model.orders.Order;

public interface OrderState {

    //---------- buequets methods start----------------

    default void showBouquet(Order order){throw new UnsupportedOperationException();}
    default void addBouquetToOrder(Order order){throw new UnsupportedOperationException();}
    default void showOrder(Order order) {throw new UnsupportedOperationException();}
    default void decorateByString(Order order) {throw new UnsupportedOperationException();}
    default void decorateByFlower(Order order) {throw new UnsupportedOperationException();}

    //---------- buequets methods finish----------------

    default void submit(Order order) {
        throw new UnsupportedOperationException();
    }

    default void grant(Order order) {
        throw new UnsupportedOperationException();
    }

    default void cancel(Order order) {
        throw new UnsupportedOperationException();
    }

    default void pay(Order order) {
        throw new UnsupportedOperationException();
    }

    default void prepareOrder(Order order) {
        throw new UnsupportedOperationException();
    }

    default void deliver(Order order) {
        throw new UnsupportedOperationException();
    }

    default void toCreatingOfBouquet(Order order) {
        throw new UnsupportedOperationException();
    }
}
