package com.group2.model.orders;

public enum Condition {
    WEDDING, FUNERAL, BIRTHDAY, VALENTINE_DAY
}
