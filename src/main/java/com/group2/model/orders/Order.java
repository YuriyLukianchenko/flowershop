package com.group2.model.orders;

import com.group2.model.bouquets.Bouquet;
import com.group2.model.bouquets.Flower;
import com.group2.model.orderStates.CreatingOfBouquet;
import com.group2.model.orderStates.OrderState;

import java.util.List;

public abstract class Order {

    private OrderState orderState = new CreatingOfBouquet();
    protected List<Bouquet> bouquets;
    private String clientId;
    private boolean delivery;
    public Bouquet currentBouquet;

    public List<Bouquet> getBouquets() {
        return bouquets;
    }

    public void setBouquets(List<Bouquet> bouquets) {
        this.bouquets = bouquets;
    }

    public Bouquet getCurrentBouquet() {
        return currentBouquet;
    }

    public void setCurrentBouquet(Bouquet currentBouquet) {
        this.currentBouquet = currentBouquet;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public abstract void showBouquet();

    public abstract void decorateByString();

    public abstract Bouquet decorateByWrap(Bouquet bouquet);

    public abstract void decorateByFlower();

    public abstract Bouquet decorateByBow(Bouquet bouquet);

    public void addBouquetToOrder() {
        this.getOrderState().addBouquetToOrder(this);
    }

    public void showOrder(){
        this.getOrderState().showOrder(this);
    }

    public void addDelivery() {

    }

    public void removeDelivery() {

    }

    private void calculatePrice() {

    }

    public void submit() {
        this.getOrderState().submit(this);
    }

    public void grant() {
        this.getOrderState().grant(this);
    }

    public void cancel() {
        this.getOrderState().cancel(this);
    }

    public void pay() {
        this.getOrderState().pay(this);
    }

    public void preparOrder() {
        this.getOrderState().prepareOrder(this);
    }

    public void deliver() {
        this.getOrderState().deliver(this);
    }

    public void toCreatingOfBouquet() {
        this.getOrderState().toCreatingOfBouquet(this);
    }
}
