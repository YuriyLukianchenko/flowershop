package com.group2.model.orders;

public class OrderFactory {

    public Order createOrser(Condition condition) {
        Order order = null;
        if (condition == Condition.WEDDING) {
            order = new WeddingOrder();
        } else if (condition == Condition.FUNERAL) {
            order = new FuneralOrder();
        } else if (condition == Condition.BIRTHDAY) {
            order = new BirthdayOrder();
        } else if (condition == Condition.VALENTINE_DAY) {
            order = new ValentinoOrder();
        }
        return order;
    }
}
