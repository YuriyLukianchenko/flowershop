package com.group2.model.orders;

import com.group2.model.bouquets.Bouquet;
import com.group2.model.bouquets.Flower;
import com.group2.model.bouquets.bouquetImplementation.WeddingBouquet;

import java.util.LinkedList;

public class WeddingOrder extends Order {

    public WeddingOrder(){
        currentBouquet = new WeddingBouquet();
        bouquets = new LinkedList<>();
    }
    @Override
    public void showBouquet() {

        this.getOrderState().showBouquet(this);
    }

    @Override
    public void decorateByString() {
        this.getOrderState().decorateByString(this);
    }

    @Override
    public Bouquet decorateByWrap(Bouquet bouquet) {
        return null;
    }

    @Override
    public void decorateByFlower() {
        this.getOrderState().decorateByFlower(this);
    }

    @Override
    public Bouquet decorateByBow(Bouquet bouquet) {
        return null;
    }
}
