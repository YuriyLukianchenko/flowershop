package com.group2.model.orders.ordersbycurrency;

import com.group2.model.currencyAdapter.moneytype.USDollar;

public class DollarsOrder implements OrderInDollars {
    private USDollar price;
    private String dollarsOrderName;

    public DollarsOrder(USDollar price, String dollarsOrderName) {
        this.price = price;
        this.dollarsOrderName = dollarsOrderName;
    }

    @Override
    public String showParameters() {
        return toString();
    }

    @Override
    public USDollar getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "DollarsOrder{" +
                "price=" + price +
                ", dollarsOrderName='" + dollarsOrderName + '\'' +
                '}';
    }
}
