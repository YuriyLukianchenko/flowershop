package com.group2.model.orders.ordersbycurrency;

import com.group2.model.currencyAdapter.moneytype.Euro;

public class EurosOrder implements OrderInEuros {
    private Euro price;
    private String eurosOrderName;

    public EurosOrder(Euro price, String eurosOrderName) {
        this.price = price;
        this.eurosOrderName = eurosOrderName;
    }

    @Override
    public String showParameters() {
        return toString();
    }

    @Override
    public Euro getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "EurosOrder{" +
                "price=" + price +
                ", eurosOrderName='" + eurosOrderName + '\'' +
                '}';
    }
}
