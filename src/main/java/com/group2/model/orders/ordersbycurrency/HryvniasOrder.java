package com.group2.model.orders.ordersbycurrency;

import com.group2.model.currencyAdapter.moneytype.UkrainianHryvnia;

public class HryvniasOrder implements OrderInHryvnias {
    private UkrainianHryvnia price;
    private String hryvniasOrderName;

    public HryvniasOrder(
            UkrainianHryvnia price, String hryvniasOrderName) {
        this.price = price;
        this.hryvniasOrderName = hryvniasOrderName;
    }

    @Override
    public String showParameters() {
        return toString();
    }

    @Override
    public UkrainianHryvnia getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "HryvniasOrder{" +
                "price=" + price +
                ", hryvniasOrderName='" + hryvniasOrderName + '\'' +
                '}';
    }
}
