package com.group2.model.orders.ordersbycurrency;

import com.group2.model.currencyAdapter.moneytype.USDollar;

public interface OrderInDollars {
    String showParameters();

    USDollar getPrice();
}
