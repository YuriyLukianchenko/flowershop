package com.group2.model.orders.ordersbycurrency;

import com.group2.model.currencyAdapter.moneytype.Euro;

public interface OrderInEuros {
    String showParameters();

    Euro getPrice();
}
