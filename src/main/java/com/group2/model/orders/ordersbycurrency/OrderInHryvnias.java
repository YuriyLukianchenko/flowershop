package com.group2.model.orders.ordersbycurrency;

import com.group2.model.currencyAdapter.moneytype.UkrainianHryvnia;

public interface OrderInHryvnias {
    String showParameters();

    UkrainianHryvnia getPrice();
}
