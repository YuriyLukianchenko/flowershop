package com.group2.view;

import com.group2.model.clientBase.ClientBase;
import com.group2.model.clientBase.DeserializationBase;
import com.group2.model.clientBase.SerializationBase;
import com.group2.model.exceptions.ClientExistAllreadyException;
import com.group2.model.exceptions.UnknownClientIdException;
import com.group2.model.orders.Condition;
import com.group2.model.orders.Order;
import com.group2.model.orders.OrderFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View {
    private Logger logger = LogManager.getLogger(View.class);
    private Scanner scanner = new Scanner(System.in);

    private void showStartInfo() {
        logger.info("---Flower Shop---");
        logger.info("(input 'help' for list of all commands)");
        logger.info("please login  or register");
    }

    private void showAllCommands() {
        logger.info("register");
        logger.info("login");
        logger.info("showAllClients");
        logger.info("showAllCommands");
    }

    public void startShopping(String clientId) {
        Order order = createOrder();
        order.setClientId(clientId);
        logger.info("STATUS: " + order.getOrderState());
        logger.info("Enter 1 to get in Order checking");
        logger.info("Enter 2 to get in Cancelling order");
        logger.info("Enter 11 to show current bouquet");
        logger.info("Enter 12 to add bouquet to order");
        logger.info("Enter 13 to order");
        logger.info("Enter 14 to decorate current Bouquet by gold string");
        logger.info("Enter 15 to decorate current Bouquet by marigold");


        do {
            try {
                int state = scanner.nextInt();
                showOrderState(order, state);
            } catch (UnsupportedOperationException e) {
                logger.info("Incorrect number");
            }
        } while (true);
    }

    private void showOrderState(Order order, int state) {
        switch (state) {
            case 1:
                order.submit();
                logger.info("Enter 2 to get in Cancelling order");
                logger.info("Enter 3 to get in Waiting for payment");
                logger.info("Enter 4 to get in Creating of bouquet");
                break;
            case 2:
                order.cancel();
                logger.info("Cancelling order");
                start();
                break;
            case 3:
                order.grant();
                logger.info("Enter 2 to get in Cancelling order");
                logger.info("Enter 4 to get in Creating of bouquet");
                logger.info("Enter 5 to get in Order preparing");
                break;
            case 4:
                order.toCreatingOfBouquet();
                logger.info("Enter 1 to get in Order checking");
                logger.info("Enter 2 to get in Cancelling order");
                break;
            case 5:
                order.pay();
                logger.info("Enter 6 to get in Shipping");
                break;
            case 6:
                order.preparOrder();
                logger.info("Enter 7 to get in Done");
                break;
            case 7:
                order.deliver();
                logger.info("Done order");
                start();
                break;
            case 11:
                order.showBouquet();
                logger.info("STATUS: " + order.getOrderState());
                logger.info("Enter 1 to get in Order checking");
                logger.info("Enter 2 to get in Cancelling order");
                logger.info("Enter 11 to show current bouquet");
                logger.info("Enter 12 to add bouquet to order");
                logger.info("Enter 13 to show order");
                logger.info("Enter 14 to decorate current Bouquet by gold string");
                logger.info("Enter 15 to decorate current Bouquet by marigold");
                break;
            case 12:
                order.addBouquetToOrder();
                logger.info("STATUS: " + order.getOrderState());
                logger.info("Enter 1 to get in Order checking");
                logger.info("Enter 2 to get in Cancelling order");
                logger.info("Enter 11 to show current bouquet");
                logger.info("Enter 12 to add bouquet to order");
                logger.info("Enter 13 to order");
                logger.info("Enter 14 to decorate current Bouquet by gold string");
                logger.info("Enter 15 to decorate current Bouquet by marigold");
                break;
            case 13:
                order.showOrder();
                logger.info("STATUS: " + order.getOrderState());
                logger.info("Enter 1 to get in Order checking");
                logger.info("Enter 2 to get in Cancelling order");
                logger.info("Enter 11 to show current bouquet");
                logger.info("Enter 12 to add bouquet to order");
                logger.info("Enter 13 to order");
                logger.info("Enter 14 to decorate current Bouquet by gold string");
                logger.info("Enter 15 to decorate current Bouquet by marigold");
                break;
            case 14:
                order.decorateByString();
                logger.info("STATUS: " + order.getOrderState());
                logger.info("Enter 1 to get in Order checking");
                logger.info("Enter 2 to get in Cancelling order");
                logger.info("Enter 11 to show current bouquet");
                logger.info("Enter 12 to add bouquet to order");
                logger.info("Enter 13 to order");
                logger.info("Enter 14 to decorate current Bouquet by gold string");
                logger.info("Enter 15 to decorate current Bouquet by marigold");
                break;
            case 15:
                order.decorateByFlower();
                logger.info("STATUS: " + order.getOrderState());
                logger.info("Enter 1 to get in Order checking");
                logger.info("Enter 2 to get in Cancelling order");
                logger.info("Enter 11 to show current bouquet");
                logger.info("Enter 12 to add bouquet to order");
                logger.info("Enter 13 to order");
                logger.info("Enter 14 to decorate current Bouquet by gold string");
                logger.info("Enter 15 to decorate current Bouquet by marigold");
                break;
            case 0:
                logger.info("STATUS: " + order.getOrderState());
                break;
            case -1:
                System.exit(0);
        }
    }

    private Order createOrder() {
        Order order = null;
        OrderFactory orderFactory = new OrderFactory();
        logger.info("Enter 1 to make a wedding order");
        logger.info("Enter 2 to make a funeral order");
        logger.info("Enter 3 to make a birthday order");
        logger.info("Enter 4 to make a valentine day order");
        int condition = scanner.nextInt();
        switch (condition) {
            case 1:
                order = orderFactory.createOrser(Condition.WEDDING);
                break;
            case 2:
                order = orderFactory.createOrser(Condition.FUNERAL);
                break;
            case 3:
                order = orderFactory.createOrser(Condition.BIRTHDAY);
                break;
            case 4:
                order = orderFactory.createOrser(Condition.VALENTINE_DAY);
                break;
            default:
                logger.info("Order has not been created");
        }
        return order;
    }

    public void start() {
        DeserializationBase deserializationBase = new DeserializationBase();
        ClientBase clientBase = deserializationBase.deserializationClientBase();
        //ClientBase clientBase = new ClientBase();
        showStartInfo();
        label:
        while (true) {
            String option = scanner.next();
            switch (option) {
                case "login":
                    logger.info("enter name");
                    String logName = scanner.next();
                    try {
                        if (clientBase.clientList.containsKey(logName)) {
                            String clientId = clientBase.clientList.get(logName).getId();
                            startShopping(clientId);
                            break label;
                        } else {
                            throw new UnknownClientIdException();
                        }
                    } catch (UnknownClientIdException e) {
                        logger.info("Unknown Client name, please register your name");
                    }

                    break;
                case "register":
                    logger.info("enter name");
                    String name = scanner.next();
                    try {
                        clientBase.addClient(name);
                        logger.info(name + " name was registered");
                        SerializationBase serializationBase = new SerializationBase();
                        serializationBase.serializationClientBase(clientBase);
                    } catch (ClientExistAllreadyException e) {
                        logger.info("Sorry, but name \"" + name + "\" already exist");
                    }

                    break;
                case "showAllClients":
                    logger.info(clientBase.clientList.toString());
                    break;
                case "help":
                    showAllCommands();
                    break;
                default:
            }
        }
    }
}
